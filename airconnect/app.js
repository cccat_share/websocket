// 引入需要的模块：http和socket.io
// 定义端口号：8080
var http = require('http'),
    io = require('socket.io'),
    port = process.env.PORT || 8080;

//创建server
var server = http.createServer(function (req, res) {
    // Send HTML headers and message
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });
    res.end('# Hello Socket Lover!');
});

// listener
// 监听 端口(8080) 事件
server.listen(port, function () {
    console.log('listening on *:' + port);
});

// 创建socket
var socket = io.listen(server);

// 添加连接监听
socket.on('connection', function (client) {

    // 连接成功则执行下面的监听
    client.on('message', function (event) {
        // Received message from client!
        console.log('message', event);
        socket.emit("message", event);
    });

    client.on('data', function (event) {
        // Received data from client!
        console.log('data', event);
        socket.emit("data", event);
    });

    // 断开连接callback
    client.on('disconnect', function () {
        console.log('Server has disconnected');
    });
});